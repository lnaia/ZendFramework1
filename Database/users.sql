CREATE  TABLE `zfw1_bootstrap`.`users` (
  `idusers` INT NOT NULL AUTO_INCREMENT ,
  `email` VARCHAR(255) NOT NULL ,
  `password` VARCHAR(255) NOT NULL ,
  `date_add` DATETIME NOT NULL ,
  PRIMARY KEY (`idusers`) ,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) );
