<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
 	protected function _initSpecialVars()
 	{
 		Zend_Session::start();
 		$registry = new Zend_Registry(array('alert' =>null));		
 		Zend_Registry::setInstance($registry);
 		
 		$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
 		Zend_Registry::set('config', $config);
 	}
 	
    protected function _initDoctype()
    {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->doctype('HTML5');
    }

    protected function _initCustom_auto_load()
    {
        $resourceLoader = new Zend_Loader_Autoloader_Resource(
            array(
                'basePath'  => APPLICATION_PATH,
                'namespace' => 'Application',
            )
        );
        $resourceLoader->addResourceType('adapter', 'adapters/', 'Adapter');
        $resourceLoader->addResourceType('cli', 'cli/', 'Cli');
    }

    protected function _initLanguage()
    {
        $adapter = new Zend_Translate(
            array(
                'adapter' => 'ini',
                'content' => BASE_PATH.'/languages/pt.ini',
                'locale'  => 'pt'
            )
        );
        $adapter->addTranslation(
            array(
                'adapter' => 'ini',
                'content' => BASE_PATH.'/languages/en.ini',
                'locale'  => 'en'
            )
        );
        $adapter->setLocale('pt');
        Zend_Registry::set('Zend_Translate', $adapter);
    }

    protected function _initRouteBind()
    {
        $front  = Zend_Controller_Front::getInstance();
        //http://framework.zend.com/manual/1.12/en/zend.controller.router.html
        $router = $front->getRouter();
        //$router->removeDefaultRoutes();
        $config = new Zend_Config_Ini(
            APPLICATION_PATH . '/configs/routes.ini', 
            'production'
        );
        
        $router->addConfig($config, 'routes');
    } 
}

