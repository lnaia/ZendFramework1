<?php

include_once('AbstractMapper.php');

class Application_Model_DBUsers extends Abstract_Mapper
{
    public function __construct()
    {
        $item = new Application_Model_DbTable_Users();
        $this->_dbTable = $item;
        $this->id = 'idusers';
    }

    public function update_fields($iditem, $params)
    {
        $this->updateInRow($iditem, $this->id, $params);
    }

    public function save_item($email, $password)
    {
        try {
            if ($email == '') {
                throw new Exception ("Email empty");
            }

            $AuxTools = new Application_Model_AuxTools();
            $password = $AuxTools->encode($password);

            $params["email"]    = $email;
            $params["password"] = $password;
            $params['date_add'] =  new Zend_Db_Expr("NOW()");
            return $this->saveInRow($params);

        } catch (Exception $e) {
            error_log($e->getMessage());
            return false;
        }
    }

    public function getSpecificItemByName($item_name)
    {
        $db = $this->_dbTable;
        $select = $db->select()->where('email = ?', $item_name);
        $result = $db->fetchRow($select);
        return $result;
    }

    public function changePass($userid, $password)
    {
        $AuxTools = new Application_Model_AuxTools();
        $password = $AuxTools->encode($password);
        $this->update_fields($userid, array('password' => $password));
    }

    public function getSpecificItem($itemid)
    {
        $db = $this->_dbTable;
        $select = $db->select()->where($this->id . ' = ?', $itemid);
        $result = $db->fetchRow($select);
        return $result;
    }


    public function deleteItem($itemid)
    {
        $db = $this->_dbTable;
        $db->delete(
            array(
                $this->id . ' = ?' => $itemid
            )
        );

        return !$this->checkItemExistsById($itemid);
    }

    public function checkItemExistsById($itemid)
    {
        $db = $this->_dbTable;
        $select = $db->select()->where($this->id . ' = ?', $itemid);
        $result = $db->fetchRow($select);
        if (is_null($result)) {
            return false;
        } else {
            return true;
        }
    }

}