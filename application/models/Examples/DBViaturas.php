<?php

include_once('AbstractMapper.php');

class Application_Model_DBViaturas extends Abstract_Mapper
{
	//write all custom code here for exammple findng by username etc

	public function __construct()
	{
		$item = new Application_Model_DbTable_Viaturas();
		$this->_dbTable = $item;
        $this->Id       = 'idviaturas';
	}

	public function save_item($title        = '',
                            $description  = '',
                            $preco        = '',
                            $ano          = '',
                            $kms          = '',
                            $marca        = '',
                            $extra    = array(),
                            $emdestaque   = 0,
                            $seotitle     = '',
                            $hidden       = 0
                            )
	{
    try{
      $AuxTools   = new Application_Model_AuxTools();
      $seotitle   = $AuxTools->clear_name($seotitle);

      $ano                        = $ano."-00-00";
      $uniqueCode                 = $this->generateUniqueCode();
      $params["title"] 		        = $title;
      $params["description"] 	    = $description;
      $params["preco"]		        = $preco;
      $params["ano"]		          = $ano;
      $params["kms"]		          = $kms;
      $params["marca"]		        = $marca;
      $params["date_add"] 		    = new Zend_Db_Expr("NOW()");
      $params["hidden"]		        = $hidden;
      $params["emdestaque"]       = $emdestaque;
      $params["seotitle"]         = $seotitle;
      $params["aux_code"]         = $uniqueCode;

      $this->saveInRow($params);
      if(!is_null($extra)){
        $this->addExtra($this->getSpecificItemByCode($uniqueCode)->idviaturas, $extra);
      }
      return $uniqueCode;

    }catch(Exception $e){
      error_log($e);
      return FALSE;

    }
 	}

    public function formSearch($filtered_params)
    {
        /*
            'extras',
            'marcas',
            'preco_inf',
            'preco_top'
         */

        $final = array();
        $db = $this->getDbTable();

        if(isset($filtered_params['extras'])){
            $temp = $this->getAllByExtraName($filtered_params['extras']);
            if(count($temp) > 0){
               $final[] = $temp[0];
            }
        }

        if(isset($filtered_params['marcas'])){
            $where[] = $db->getAdapter()->quoteInto("marca like ?", $filtered_params['marcas']);
        }


        /******* PRECO ********/
        if(
            isset($filtered_params['preco_inf']) ||
            isset($filtered_params['preco_top'])
        ){
            $a = ((isset($filtered_params['preco_inf']))?$filtered_params['preco_inf']:0);
            $b = ((isset($filtered_params['preco_top']))?$filtered_params['preco_top']:999999);

            $where[] = $db->getAdapter()->quoteInto("preco >= ?", $a);
            $where[] = $db->getAdapter()->quoteInto("preco <= ?", $b);
        }

        /******* ANO ********/
        if(
            isset($filtered_params['ano_inf']) ||
            isset($filtered_params['ano_top'])
        ){
            $a = ((isset($filtered_params['ano_inf']))?substr($filtered_params['ano_inf'],0,4):0001);
            $b = ((isset($filtered_params['ano_top']))?substr($filtered_params['ano_top'],0,4):9999);

            $where[] = $db->getAdapter()->quoteInto("ano >= ?", $a."-00-00");
            $where[] = $db->getAdapter()->quoteInto("ano <= ?", $b."-00-00");
        }


        /******* KMS ********/
        if(
            isset($filtered_params['kms_inf']) ||
            isset($filtered_params['kms_top'])
        ){
            $a = ((isset($filtered_params['kms_inf']))?$filtered_params['kms_inf']:0);
            $b = ((isset($filtered_params['kms_top']))?$filtered_params['kms_top']:999999);

            $where[] = $db->getAdapter()->quoteInto("kms >= ?", $a);
            $where[] = $db->getAdapter()->quoteInto("kms <= ?", $b);
        }

        $result = $db->fetchAll($where);

        foreach($result as $item){
            if(!$this->exists_viatura($final, $item->idviaturas)){
                $final[] = $this->getSpecificItem($item->idviaturas);
            }
        }

        return $final;
    }

    private function exists_viatura($final, $idviaturas)
    {
        foreach($final as $obj)
        {
            if($obj->idviaturas == $idviaturas){
                return true;
            }
        }
        return false;
    }

  public function addExtra($itemid, Array $array)
  {
    $viaCatRelation = new Application_Model_DBViaturasExtras();
    foreach($array as $obj){
      $viaCatRelation->save_item($obj,$itemid);
    }
  }

  /*
   *  @Deprecated
   */
 	public function listAllVisible($offset = 0, $rows = 10)
 	{
 	  die('no need to deal with hiddeno');
 		/*
 		$db = $this->getDbTable();
 		$where[] = $db->getAdapter()->quoteInto('hidden = ?', 0);
 		$result = $db->fetchAll($where);
 		*/

 		$db       = $this->getDbTable();
 		$select   = $db->select()->where('hidden = ?', 0)->order(array($this->Id . ' DESC'));
        $viaturas = $db->fetchAll($select);
        $result   = array();

        foreach($viaturas as $item){
          $result[] = array(
            'viatura'     => $item,
            'Extras'  => $this->getAllCategoriesByViatura($item->idviaturas)
          );
        }

 		return $result;
 	}

  //@WARNING it only returns cars that have pictures. No picture, no fun!
  public function listAllEmDestaque()
  {

    $db       = $this->getDbTable();
    $select   = $db->select()->where('emdestaque = ?', 1)->order(array($this->Id . ' DESC'));
    $viaturas = $db->fetchAll($select);
    $result   = array();

    foreach($viaturas as $item){
      $temp_item = $this->getSpecificItem($item->idviaturas);
      if(count($temp_item->fotos)> 0) {
        $result[] = $temp_item;
      }
    }

    return $result;
  }

  public function listAll($order = 1)
  {
    /*
    * ORDER:
    * 1 - inverse to ID
    * 2 - by title
    * 3 - by marca
    * 4 - by ano
    * 5 - by kms
    * 6 - by preco
    */
    if(!isset($order) || is_null($order) || $order <= 0 || $order > 6){
      $order = 1;
    }

    $db       = $this->getDbTable();
    switch($order){
      case ($order == 1):
        $select   = $db->select()->order(array($this->Id . ' DESC'));
        break;
      case ($order == 2):
        $select   = $db->select()->order(array('title ' . ' DESC'));
        break;
      case ($order == 3):
        $select   = $db->select()->order(array('marca ' . ' DESC'));
        break;

      case ($order == 4):
        $select   = $db->select()->order(array('ano ' . ' DESC'));
        break;
      case ($order == 5):
        $select   = $db->select()->order(array('kms ' . ' DESC'));
        break;
      case ($order == 6):
        $select   = $db->select()->order(array('preco ' . ' DESC'));
        break;
    }

    $viaturas = $db->fetchAll($select);
    $result   = array();

    foreach($viaturas as $item){
      $result[] = $this->getSpecificItem($item->idviaturas);

    }

    return $result;
  }


  /*
   *
   * hidden is deprecated
   */
  public function getAllByExtra($extraid)
  {
    $viaCatRelation = new Application_Model_DBViaturasExtras();
    $viaturas       = $viaCatRelation->getByExtra($extraid);
    $result         = array();

    foreach($viaturas as $item){
        $viatura_single = $this->getSpecificItem($item->idviatura_rel);
        $result[] =  $viatura_single;
    }
    return $result;
  }

  public function getAllByExtraName($category_name)
  {
    $categories     = new Application_Model_DBExtras();
    $category       = $categories->getSpecificItemByName($category_name);
    $result         = $this->getAllByExtra($category->idextras);
    return $result;
  }

  /*
   *
   */
  private function getAllCategoriesByViatura($idviatura){

    $viaCatRelation = new Application_Model_DBViaturasExtras();
    $categoryDB     = new Application_Model_DBExtras();

    $category_ids   = $viaCatRelation->getByViatura($idviatura);
    $result         = array();

    foreach($category_ids as $id){
      $result[] = $categoryDB->getSpecificItem($id->idextra_rel);
    }
    return $result;
  }

  /*
   * @TODO test
   *
   */
  public function getTopVisibleItems($rows = 3)
  {
    $db       = $this->getDbTable();
    $query 	  = "SELECT * FROM viaturas WHERE hidden = 0 ORDER BY date_add DESC limit 0, ".$rows;
    $viaturas = $db->getDefaultAdapter()->query($query)->fetchAll();

    $result   = array();

    foreach($viaturas as $item){
      $viatura_single = $this->getSpecificItem($item['idviaturas']);
      $result[] =  $viatura_single;
    }

    return $result;
  }
 	
 	public function update_fields($iditem, $params)
 	{
    if(isset($params['ano'])){
      $params['ano'] = $params['ano'] . "-00-00";
    }
 		$this->updateInRow($iditem, $this->Id , $params);
 	
 	}

  public function updateextra($itemid, $category)
  {
    $DBViaturasExtras = new Application_Model_DBViaturasExtras();
    $DBExtras         = new Application_Model_DBExtras();

    $DBViaturasExtras->deleteItemByViatura($itemid);
    foreach($category as $obj)
    {
      //se a extra existir adiciona à tabela das relações
      if($DBExtras->checkItemExistsById($obj)) {
        $DBViaturasExtras->save_item($obj,$itemid);
      }
    }
  }
 	
 	public function findItem($str)
 	{
         die('not done yet, you need to pass by getspecifict...');
 		$str="%".$str."%";
 	
 		$db = $this->getDbTable();
 		$where[] = $db->getAdapter()->quoteInto("title like ?", $str);
 		$result = $db->fetchAll($where);
 		
 		if(is_null($result))
 		{
 			$db = $this->getDbTable();
 			$where[] = $db->getAdapter()->quoteInto("description like ?", $str);
 			$result = $db->fetchAll($where);
  		}
 		return $result;
 	}

    public function findItemByMarca($str)
    {
        $str="%".$str."%";

        $db = $this->getDbTable();
        $where[] = $db->getAdapter()->quoteInto("marca like ?", $str);
        $result = $db->fetchAll($where);

        $final = array();
        foreach($result as $item){
            $viatura_single = $this->getSpecificItem($item->idviaturas);
            $final[] =  $viatura_single;
        }

        return $final;
    }
 	
 	public function deleteItem($itemid)
 	{
 		$db = $this->_dbTable;
 		$db->delete(array(
      $this->Id .' = ?' => $itemid
 		));
 		return !$this->checkItemExistsById($itemid);
 	}
 	
 	private function checkItemExistsById($itemid)
 	{
 		$db = $this->_dbTable;
 		$select = $db->select()->where($this->Id .' = ?', $itemid);
 		$result = $db->fetchRow($select);
 		if(is_null($result))
 		{
 			return false;
 		}
 		else
 		{
 			return true;
 		}
 	}

  public function total()
  {
    $db = $this->getDbTable();
    $select = $db->select()->from($db, array('count(*) as amount'));
    $result = $db->fetchRow($select);
    return $result->amount;
  }
 	
 	public function getSpecificItem($itemid)
 	{
        $db     = $this->_dbTable;
 		$select = $db->select()->where($this->Id .' = ?', $itemid);
 		$result = $db->fetchRow($select);

    $DBViaturasExtras = new Application_Model_DBViaturasExtras();
    $DBExtras         = new Application_Model_DBExtras();
    $DBViaturasFotos      = new Application_Model_DBViaturasFotos();

    $cat                  = $DBViaturasExtras->getByViatura($itemid);
    $temp                 = array();

    /*idextras, description, title, date_add*/
    foreach($cat as $item){
      $obj        = new stdClass();
      $single_cat = $DBExtras->getSpecificItem($item->idextra_rel);

      $obj->idextras  = $single_cat->idextras;
      $obj->description   = $single_cat->description;
      $obj->title         = $single_cat->title;
      $obj->date_add      = $single_cat->date_add;
      $obj->seotitle      = $single_cat->seotitle;
      $temp[]             = $obj;
    }
    $cat_temp             = $temp;

    /*
     * Pictures
     */
    $pictures   = $DBViaturasFotos->getByViatura($itemid);
    $fotos_temp = array();

    //idgallery, idviatura, title, description, date, filename, hidden
    foreach($pictures as $obj){
      $temp = new stdClass();
      $temp->idgallery    = $obj->idgallery;
      $temp->idviatura    = $obj->idviatura;
      $temp->title        = $obj->title;
      $temp->description  = $obj->description;
      $temp->date         = $obj->date;
      $temp->filename     = $obj->filename;
      $temp->hidden       = $obj->hidden;
      $fotos_temp[]       = $temp;
    }
    $fotos                = $fotos_temp;//

    /*
     * idviaturas, title, description, preco, ano, date_add, kms, marca, hidden, aux_code, emdestaque
     */
    $object = new stdClass();
    $object->idviaturas   = $result->idviaturas;
    $object->title        = $result->title;
    $object->seotitle     = $result->seotitle;
    $object->description  = $result->description;
    $object->preco        = $result->preco;
    $object->ano          = $result->ano;
    $object->date_add     = $result->date_add;
    $object->kms          = $result->kms;
    $object->marca        = $result->marca;
    $object->hidden       = $result->hidden;
    $object->aux_code     = $result->aux_code;
    $object->emdestaque   = $result->emdestaque;
    $object->extras       = $cat_temp;
    $object->fotos        = $fotos;


    return $object;
 	}

  public function getSpecificItemByCode($item_generated_code)
  {
    $db = $this->_dbTable;
    $select = $db->select()->where('aux_code = ?', $item_generated_code);
    $result = $db->fetchRow($select);
    return $result;
  }


  public function generateUniqueCode(){
    return crc32($this->drupal_random_bytes(150));
  }

  public function drupal_random_bytes($count) {
    // $random_state does not use drupal_static as it stores random bytes.
    static $random_state, $bytes, $php_compatible;
    // Initialize on the first call. The contents of $_SERVER includes a mix of
    // user-specific and system information that varies a little with each page.
    if (!isset($random_state)) {
      $random_state = print_r($_SERVER, TRUE);
      if (function_exists('getmypid')) {
        // Further initialize with the somewhat random PHP process ID.
        $random_state .= getmypid();
      }
      $bytes = '';
    }
    if (strlen($bytes) < $count) {
      // PHP versions prior 5.3.4 experienced openssl_random_pseudo_bytes()
      // locking on Windows and rendered it unusable.
      if (!isset($php_compatible)) {
        $php_compatible = version_compare(PHP_VERSION, '5.3.4', '>=');
      }
      // /dev/urandom is available on many *nix systems and is considered the
      // best commonly available pseudo-random source.
      if ($fh = @fopen('/dev/urandom', 'rb')) {
        // PHP only performs buffered reads, so in reality it will always read
        // at least 4096 bytes. Thus, it costs nothing extra to read and store
        // that much so as to speed any additional invocations.
        $bytes .= fread($fh, max(4096, $count));
        fclose($fh);
      }
      // openssl_random_pseudo_bytes() will find entropy in a system-dependent
      // way.
      elseif ($php_compatible && function_exists('openssl_random_pseudo_bytes')) {
        $bytes .= openssl_random_pseudo_bytes($count - strlen($bytes));
      }
      // If /dev/urandom is not available or returns no bytes, this loop will
      // generate a good set of pseudo-random bytes on any system.
      // Note that it may be important that our $random_state is passed
      // through hash() prior to being rolled into $output, that the two hash()
      // invocations are different, and that the extra input into the first one -
      // the microtime() - is prepended rather than appended. This is to avoid
      // directly leaking $random_state via the $output stream, which could
      // allow for trivial prediction of further "random" numbers.
      while (strlen($bytes) < $count) {
        $random_state = hash('sha256', microtime() . mt_rand() . $random_state);
        $bytes .= hash('sha256', mt_rand() . $random_state, TRUE);
      }
    }
    $output = substr($bytes, 0, $count);
    $bytes = substr($bytes, $count);
    return base64_encode($output);
  }

  public function getMarcas($array_only = FALSE)
  {
    $default = "Alfa Romeo, Aston Martin, Audi, Austin Morris, Bentley, BMW, Chevrolet, Chrysler, Citroën, Dacia, Daewoo, Daihatsu, Dodge, Ferrari, Fiat, Ford, GMC, Honda, Hummer, Hyundai, Isuzu, Jaguar, Jeep, Kia, Lada, Lamborghini, Lancia, Land Rover, Lexus, Lotus, Maserati, Mazda, Mercedes-Benz, MG, MINI, Mitsubishi, Nissan, Opel, Peugeot, Porsche, Renault, Rolls Royce, Rover, Saab, Seat, Skoda, Smart, SsangYong, Subaru, Suzuki, Tata, Toyota, UMM, Vauxhall, Volvo, VW";

    $default = explode(",", $default);
    $marca_bd = array();
    $carros = $this->listAll();
    foreach($carros as $carr){
      $marca_bd[] = $carr->marca;
    }

    $final = array_merge($default,$marca_bd);
    $final = array_unique($final);
    asort($final);

    foreach($final as $k => $v){
      if($v == ''){
        unset($final[$k]);
      }else{
          if($array_only){
              $final[$k] = $v;
          }else{
              $final[$k] = "&quot;" . $v . "&quot;";
          }
      }
    }

    if($array_only){
        return $final;
    }else{
        $final = implode(",", $final);
        return $final;
    }
  }

}