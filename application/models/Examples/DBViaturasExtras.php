<?php

include_once('AbstractMapper.php');

class Application_Model_DBViaturasExtras extends Abstract_Mapper
{
	//write all custom code here for exammple findng by username etc

	public function __construct()
	{
		$item = new Application_Model_DbTable_ViaturasExtras();
		$this->_dbTable = $item;
	}

	public function save_item($idextra_rel, $idviatura_rel)
	{
    try
    {

      if(!$this->checkItemExistsById($idextra_rel, $idviatura_rel)) {
        $params["idextra_rel"]      = $idextra_rel;
        $params["idviatura_rel"]    = $idviatura_rel;
        $this->saveInRow($params);
      }
      return TRUE;

    }
    catch( Exception $e )
    {
      return FALSE;
    }
 	}

  public function listAll()
  {

    $db = $this->getDbTable();
    $result = $db->fetchAll();

    return $result;
  }


  public function getByExtra($extra)
 	{
 		$db = $this->getDbTable();
 		$select = $db->select()->where('idextra_rel = ?', $extra);
 		$result = $db->fetchAll($select);
 		return $result;
 	}

  public function getByViatura($viatura)
  {
    $db = $this->getDbTable();
    $select = $db->select()->where('idviatura_rel = ?', $viatura);
    $result = $db->fetchAll($select);
    return $result;
  }

  public function deleteItemByViatura($idviatura_rel)
  {
    $db = $this->_dbTable;
    $db->delete(
      array(
      'idviatura_rel = ?'   => $idviatura_rel
      )
    );
  }

 	public function deleteItem($idextra_rel, $idviatura_rel)
 	{
 		$db = $this->_dbTable;
 		$db->delete(array(
          'idextra_rel = ?' => $idextra_rel,
          'idviatura_rel = ?'   => $idviatura_rel,
 		));
 		return !$this->checkItemExistsById($idextra_rel,$idviatura_rel);
 	}

  /*
   * @TODO Testar isto!!!!!
   */
 	private function checkItemExistsById($idextra_rel,$idviatura_rel)
 	{
 		$db = $this->_dbTable;
 		$select = $db->select()
          ->where('idextra_rel = ?', $idextra_rel)
          ->where('idviatura_rel = ?', $idviatura_rel);
 		$result = $db->fetchRow($select);
 		if(is_null($result))
 		{
 			return false;
 		}
 		else
 		{
 			return true;
 		}
 	}

  /*
   * @TODO Testar isto!!!!!
   */
 	public function getSpecificItem($idextra_rel,$idviatura_rel)
 	{
 		$db = $this->_dbTable;
        $select = $db->select()
          ->where('idextra_rel = ?', $idextra_rel)
          ->where('idviatura_rel = ?', $idviatura_rel);
 		$result = $db->fetchRow($select); 	
 		return $result;
 	}
 	 
}