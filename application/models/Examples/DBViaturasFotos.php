<?php

include_once('AbstractMapper.php');

class Application_Model_DBViaturasFotos extends Abstract_Mapper
{
	//write all custom code here for exammple findng by username etc

	public function __construct()
	{
		$item = new Application_Model_DbTable_ViaturasFotos();
		$this->_dbTable = $item;
	}

	public function save_item($idviatura, $title = '', $description = '', $filename, $hidden = 0)
	{
    try{


      $params["idviatura"]    = $idviatura;
      $params["title"] 	      = $title;
      $params["description"]  = $description;
      $params["filename"]     = $filename;
      $params["hidden"]       = $hidden;
      $params['date']         = new Zend_Db_Expr("NOW()");
      $this->saveInRow($params);
      return TRUE;

    }catch ( Exception $e){
      error_log($e);
      return FALSE;
    }
 	}

  public function listAll()
  {

    $db = $this->getDbTable();
    $result = $db->fetchAll();

    return $result;
  }

  public function update_fields($iditem, $params)
  {
    $this->updateInRow($iditem, 'idgallery' , $params);

  }
  public function getByViatura($viatura)
  {
    $db = $this->getDbTable();
    $select = $db->select()->where('idviatura = ?', $viatura);
    $result = $db->fetchAll($select);
    return $result;
  }

 	public function deleteItem($itemid)
 	{
    $item = $this->getSpecificItem($itemid);
    $itemPath = $item->filename;
    $itemPath = "/img/uploads/".$itemPath;

 		$db = $this->_dbTable;
 		$db->delete(array(
      'idgallery = ?' => $itemid,
 		));
    @unlink($itemPath);
 		return !$this->checkItemExistsById($itemid);
 	}

  /*
   * @TODO Testar isto!!!!!
   */
 	private function checkItemExistsById($itemid)
 	{
 		$db = $this->_dbTable;
 		$select = $db->select()
      ->where('idgallery = ?', $itemid);

 		$result = $db->fetchRow($select);
 		if(is_null($result))
 		{
 			return false;
 		}
 		else
 		{
 			return true;
 		}
 	}

  /*
   * @TODO Testar isto!!!!!
   */
 	public function getSpecificItem($itemid)
 	{
 		$db = $this->_dbTable;
    $select = $db->select()
      ->where('idgallery = ?', $itemid);
 		$result = $db->fetchRow($select); 	
 		return $result;
 	}
 	 
}