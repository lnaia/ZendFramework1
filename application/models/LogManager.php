<?php

class Application_Model_LogManager
{
    protected $_loggerError;
    protected $_loggerSuccess;

    public function __construct()
    {
        $logs = APPLICATION_PATH . "/logs/";
        $date = date('Y_m_d');

        $writerE = new Zend_Log_Writer_Stream($logs."_".__CLASS__.$date.".log");
        $this->_loggerError = new Zend_Log($writerE);

        $writerS = new Zend_Log_Writer_Stream($logs."_".__CLASS__.$date.".log");
        $this->_loggerSuccess = new Zend_Log($writerS);
    }

    public function logme($str, $errors = null)
    {
        $str = Application_Model_AuxTools::get_contents($str);
        if($errors) {
            $this->_loggerError->err($str);
        } else {
            $this->_loggerSuccess->info($str);
        }
    }
}
