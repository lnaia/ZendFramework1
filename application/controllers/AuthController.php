<?php

class AuthController extends Zend_Controller_Action
{
	protected $_sessionObj;

	public function init()
	{
		$this->_sessionObj = new Zend_Session_Namespace('barebones');
	}

	public function logoutAction()
	{
		Zend_Session::destroy(true);
		$this->_redirect('/');
	}

	public function loginAction()
	{
		if ($this->getRequest()->isPost()){
			$adapter = new Application_Adapter_BarebonesAuthAdapter();
			$adapter->init(
                $this->getRequest()->getParam('email'),
                $this->getRequest()->getParam('password')
			);

			$auth           = Zend_Auth::getInstance();
			$rememberField  = $this->getRequest()->getParam('remember');
			if(isset($rememberField)) {
				$seconds  = 60 * 60 * 24 * 7; // 7 days
				Zend_Session::RememberMe($seconds);
			}

			$result = $auth->authenticate($adapter);
 			if ($result->isValid()) {
				$this->_sessionObj->user = $result->getIdentity();
				$this->_redirect('/admin');
			} else {
				$this->_sessionObj->_alertMsg = $this->view->translate('bad_email_password');
				$this->_redirect('/admin/login');
 			}
		}
		$this->_sessionObj->_alertMsg = $this->view->translate('incomplete_email_password');
		$this->_redirect('/admin/login');
	}
}
