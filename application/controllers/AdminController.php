<?php

class AdminController extends Zend_Controller_Action
{
	protected $_sessionObj;
    protected $_aux;
	
	public function init()
	{
		$this->_sessionObj  = new Zend_Session_Namespace('barebones');
        $this->_aux         = new Application_Model_AuxTools();
	}

    public function postDispatch()
    {
        if (!isset($this->_sessionObj->user))
        {
            if(strcmp($this->getRequest()->action,"login")!=0)
            {
                $this->_sessionObj->_alertMsg = $this->_aux->set_alert("error",$this->view->translate('need_login'));
                $this->_redirect('/admin/login');
            }
        }
    }

	public function indexAction()
	{
        $this->view->customKeywords     = "Palavras chave relacionadas como content";
        $this->view->customDescription  = "Descrição relacionada com o content";
	}

    public function loginAction()
    {

    }

}

